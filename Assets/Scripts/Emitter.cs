﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = System.Random;

namespace Assets.Scripts
{
    public class Emitter : MonoBehaviour, IDragHandler, IPointerUpHandler
    {
        [SerializeField]
        private GameFieldView _gameField;
        [SerializeField]
        private Camera      _camera;

        private     float     _minValue = -1f;
        private     float     _maxValue = 1f;
        private     Contexts  _contexts = Contexts.sharedInstance;
        private     int       _nextValue = 16;
        Random random = new Random();
        private int[] _initialValues = new[] { 2, 4, 8, 16, 32 };

        void Start()
        {
            Debug.DrawLine(Vector3.zero, Vector3.one, Color.red, Mathf.Infinity);
            
        }

        public void OnDrag(PointerEventData eventData)
        {
            var pointB = _camera.ScreenToWorldPoint(eventData.pointerCurrentRaycast.screenPosition);
            Vector3 dir = (pointB - transform.position) * -1f;
            _gameField.SetDirection(dir);
            var foundHit = Physics2D.Raycast(transform.position, dir * 10, 10, 1 << 8);
            _contexts.game.ReplaceTarget(foundHit.transform.gameObject, _nextValue);
            
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!_contexts.game.isReadyToShot)
                return;
            var e = _contexts.game.GetGroup(GameMatcher.Target).GetSingleEntity();
            e.AddMakeShot(Vector3.zero);
            _nextValue = (int)_initialValues.GetValue(random.Next(_initialValues.Length));
        }
    }
}

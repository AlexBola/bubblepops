﻿using System;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Unique]
public class GridSizeComponent : IComponent
{
    public int Rows;
    public int Columns;
}

[Game]
public class BubbleComponent : IComponent
{
    public  Guid    Key;
    public  Color   Color;
    public  int     Value;
    public  int     Row;
    public  int     Column;
}

[Game, Unique]
public class GameStateComponent : IComponent
{
    public GameState CurrentState;
}

[Game]
public class MakeShotComponent : IComponent
{
    public Vector3 Direction;
}

[Game]
public class MakeBubbleComponent : IComponent
{
    public int Row;
    public int Column;
    public int Value;
}

[Game, Unique]
public class MakeWaveComponent : IComponent
{
    public int Step;
}

[Game, Unique]
public class ReadyToShotComponent : IComponent
{}

[Game, Unique]
public class TargetComponent : IComponent
{
    public GameObject HitObject;
    public int Value;
}
[Game]
public class BlowComponent : IComponent
{
}

//[Game]
//public class MergeComponent : IComponent
//{
//}

[Game, Unique]
public class CheckForMatchingComponent : IComponent
{
}

[Game]
public class MatchedComponent : IComponent
{
}

[Game, Unique]
public class MatchingCompleteComponent : IComponent
{
}

[Game]
public class ReadyToDestroyComponent : IComponent
{
}

public enum GameState
{
    Play,
    Stop,
    Paused,
    End
}
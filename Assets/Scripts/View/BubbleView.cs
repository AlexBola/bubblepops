﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class BubbleView : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro _label;

    public Guid ViewId { private set; get; }
    public CircleCollider2D Collider { private set; get; }

    private SpriteRenderer _spriteRenderer;
    private List<MatchingData<GameEntity>> _subscriptions;
    private Contexts _contexts;

    private float _bound;

    private Vector2 _rootPosition;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        Collider = GetComponent<CircleCollider2D>();
        _bound = Collider.radius * 2;
        _contexts = Contexts.sharedInstance;
    }

    public void Init(GameEntity entity, Vector2 rootPos)
    {
        ViewId = entity.bubble.Key;
        _rootPosition = rootPos;
        Subscribe();
        UpdateLabel(entity);
    }

    public void Destroy()
    {
        Unsubscribe();
        _spriteRenderer.color = Color.gray;
    }

    private void Subscribe()
    {
        _subscriptions = new List<MatchingData<GameEntity>>()
        {new MatchingData<GameEntity>
        {
            Matcher = GameMatcher.Bubble,
            Filter = (e) => e.hasBubble && !e.hasTarget && ViewId == e.bubble.Key,
            UpdateView = OnBubbleUpdate
        }};
        _contexts.view.CreateEntity().AddViewListener(_subscriptions);
    }

    private void Unsubscribe()
    {
        foreach (ViewEntity entity in _contexts.view.GetGroup(ViewMatcher.ViewListener).GetEntities().Where( e => e.viewListener.Observables == _subscriptions))
        {
            entity.Destroy();
        }
    }

    private void OnBubbleUpdate(GameEntity entity)
    {
        UpdateLabel(entity);
        var y = _rootPosition.y - _bound * 0.9f * entity.bubble.Row;
        transform.DOMoveY(y, 0.3f);
    }

    private void UpdateLabel(GameEntity entity)
    {
        _spriteRenderer.color = entity.hasTarget ? new Color(entity.bubble.Color.r, entity.bubble.Color.g, entity.bubble.Color.b, 0.5f) : entity.bubble.Color;
        if (!entity.hasTarget)
            _label.text = entity.bubble.Value + "";
        else
            _label.text = "";
    }
}

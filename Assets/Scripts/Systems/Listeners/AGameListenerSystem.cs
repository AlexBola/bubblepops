﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;

namespace Assets.Scripts.Systems.Listeners
{
    public class AGameListenerSystem : ReactiveSystem<GameEntity>
    {
        protected readonly Contexts _contexts;

        public AGameListenerSystem(Contexts c) : base(c.game)
        {
            _contexts = c;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            throw new NotImplementedException();
        }

        protected override bool Filter(GameEntity entity)
        {
            throw new NotImplementedException();
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity entity in entities)
            {
                IEnumerable<IEnumerable<MatchingData<GameEntity>>> observers = _contexts.view.GetGroup(ViewMatcher.ViewListener).GetEntities().Where( e => e.isEnabled).Select(e => e.viewListener.Observables.Where(m => (m.Matcher) != null && m.Filter(entity)));

                //Debug.Log(observers);
                foreach (IEnumerable<MatchingData<GameEntity>> viewEntity in observers)
                {
                    foreach (MatchingData<GameEntity> matchingData in viewEntity)
                    {
                        matchingData.UpdateView(entity);
                    }
                }
            }
        }
    }
}

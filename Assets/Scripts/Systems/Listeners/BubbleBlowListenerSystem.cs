﻿using Assets.Scripts.Systems.Listeners;
using Entitas;

public class BubbleBlowListenerSystem : AGameListenerSystem
{
    public BubbleBlowListenerSystem(Contexts c) : base(c)
    {}

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Blow);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }
}

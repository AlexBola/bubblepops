﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

public class AddTargetSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts _contexts;

    public AddTargetSystem(Contexts c) : base(c.game)
    {
        _contexts = c;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Target);
    }

    protected override bool Filter(GameEntity entity)
    {
        return _contexts.game.isReadyToShot;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (GameEntity entity in entities)
        {
            var view = entity.target.HitObject.GetComponent<BubbleView>();
            if (view == null)
                return;
            var collisionE = _contexts.game.GetGroup(GameMatcher.Bubble).GetEntities().FirstOrDefault(e => e.bubble.Key == view.ViewId);
            if (collisionE == null)
                return;
            if (!entity.hasBubble)
                entity.AddMakeBubble(collisionE.bubble.Row + 1, collisionE.bubble.Column, entity.target.Value);
            else
                entity.ReplaceBubble(entity.bubble.Key, entity.bubble.Color, entity.bubble.Value, collisionE.bubble.Row+1, collisionE.bubble.Column);
        }
    }
}

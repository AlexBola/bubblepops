﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scripts.Systems.Gameplay
{
    public class CreateBubbleSystem : ReactiveSystem<GameEntity>
    {
        private readonly Contexts _contexts;

        private static readonly Color[] Colors = new Color[]
        {
            "f05235".GetColorFromString(), "49cca0".GetColorFromString(),
            "e8678e".GetColorFromString(), "febb2f".GetColorFromString(),
            "e18242".GetColorFromString()
        };

        private readonly Dictionary<int, Color> _refToColors;

        public CreateBubbleSystem(Contexts c) : base(c.game)
        {
            _contexts = c;

            _refToColors = new Dictionary<int, Color>()
            {
                {   2,  Colors[(int) BubbleColor.Red]    },
                {   4,  Colors[(int) BubbleColor.Green]  },
                {   8,  Colors[(int) BubbleColor.Pink]   },
                {   16, Colors[(int) BubbleColor.Yellow] },
                {   32, Colors[(int) BubbleColor.Ginger] },
            };
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.MakeBubble);
        }

        protected override bool Filter(GameEntity entity)
        {
            return true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity entity in entities)
            {
                Color bubbleColor = _refToColors[entity.makeBubble.Value];
                entity.AddBubble(Guid.NewGuid(), bubbleColor, entity.makeBubble.Value, entity.makeBubble.Row, entity.makeBubble.Column);
            }
        }
    }

    public enum BubbleColor
    {
        Red,
        Green,
        Pink,
        Yellow,
        Ginger
    }
}

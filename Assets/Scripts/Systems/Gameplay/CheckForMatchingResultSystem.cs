﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;
using UniRx;
using UnityEngine;

namespace Assets.Scripts.Systems.Gameplay
{
    public class CheckForMatchingResultSystem : ReactiveSystem<GameEntity>
    {
        private readonly Contexts _contexts;

        public CheckForMatchingResultSystem(Contexts c) : base(c.game)
        {
            _contexts = c;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.MatchingComplete);
        }

        protected override bool Filter(GameEntity entity)
        {
            return true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            _contexts.game.isMatchingComplete = false;

            var matchesEntities = _contexts.game.GetGroup(GameMatcher.Bubble).GetEntities().Where(e => e.isMatched).ToArray();

            foreach (GameEntity entity in matchesEntities)
            {
                entity.isBlow = true;
                //entity.ReplaceBubble(entity.bubble.Key, Color.gray, entity.bubble.Value, entity.bubble.Row,
                //    entity.bubble.Column);
            }
            if (matchesEntities.Length < 1)
                _contexts.game.ReplaceMakeWave(1);

            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(_ => _contexts.game.isReadyToShot = true);
        }
    }
}

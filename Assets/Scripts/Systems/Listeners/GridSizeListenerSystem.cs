﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Systems.Listeners;
using Entitas;

public class GridSizeListenerSystem : AGameListenerSystem
{
    public GridSizeListenerSystem(Contexts c) : base(c)
    {}

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.GridSize);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity readyToShotEntity { get { return GetGroup(GameMatcher.ReadyToShot).GetSingleEntity(); } }

    public bool isReadyToShot {
        get { return readyToShotEntity != null; }
        set {
            var entity = readyToShotEntity;
            if (value != (entity != null)) {
                if (value) {
                    CreateEntity().isReadyToShot = true;
                } else {
                    entity.Destroy();
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly ReadyToShotComponent readyToShotComponent = new ReadyToShotComponent();

    public bool isReadyToShot {
        get { return HasComponent(GameComponentsLookup.ReadyToShot); }
        set {
            if (value != isReadyToShot) {
                var index = GameComponentsLookup.ReadyToShot;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : readyToShotComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherReadyToShot;

    public static Entitas.IMatcher<GameEntity> ReadyToShot {
        get {
            if (_matcherReadyToShot == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.ReadyToShot);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherReadyToShot = matcher;
            }

            return _matcherReadyToShot;
        }
    }
}

﻿using Assets.Scripts.Systems.Listeners;
using Entitas;

public class MakeWaveListenerSystem : AGameListenerSystem
{
    public MakeWaveListenerSystem(Contexts c) : base(c) {}

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.MakeWave);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }
}

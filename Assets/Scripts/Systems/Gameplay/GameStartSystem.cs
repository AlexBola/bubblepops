﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entitas;

public class GameStartSystem : ReactiveSystem<GameInfoEntity>
{
    private readonly Contexts _contexts;

    public GameStartSystem(Contexts c): base(c.gameInfo)
    {
        _contexts = c;
    }

    protected override ICollector<GameInfoEntity> GetTrigger(IContext<GameInfoEntity> context)
    {
        return context.CreateCollector(GameInfoMatcher.GameScreen);
    }

    protected override bool Filter(GameInfoEntity entity)
    {
        return entity.gameScreen.CurrentScreen == GameScreen.Gameplay;
    }

    protected override void Execute(List<GameInfoEntity> entities)
    {
        //_contexts.game.ReplaceGameState(GameState.Play);
    }
}

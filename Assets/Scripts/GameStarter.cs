﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Systems;
using Assets.Scripts.Systems.Gameplay;
using Entitas;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
    private Systems _systems;
    private Contexts _contexts;

    void Awake()
    {
        _contexts = Contexts.sharedInstance;
    }

    
    void Start()
    {
        _systems = new Feature("Systems");
        _systems.Add(new ViewListenersFeature(_contexts));
        _systems.Add(new GameplayFeature(_contexts));

        _systems.Initialize();
    }

    void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }
}

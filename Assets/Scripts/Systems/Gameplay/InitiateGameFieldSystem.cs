﻿using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scripts.Systems.Gameplay
{
    public class InitiateGameFieldSystem : ReactiveSystem<GameEntity>
    {
        private readonly Contexts _contexts;

        private readonly Color[] _colors = new Color[]
        {
            "f05235".GetColorFromString(), "49cca0".GetColorFromString(),
            "e8678e".GetColorFromString(), "febb2f".GetColorFromString(),
            "e18242".GetColorFromString()
        };

        public InitiateGameFieldSystem(Contexts c) : base(c.game)
        {
            _contexts = c;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.GridSize);
        }

        protected override bool Filter(GameEntity entity)
        {
            return true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            Random random = new Random();
           
            int[] initialValues = new[] {2, 4, 8, 16, 32};
        
            foreach (var entity in entities)
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < entity.gridSize.Columns; j++)
                    {
                        //Color bubbleColor = (Color)_colors.GetValue(random.Next(_colors.Length));
                        int value = (int)initialValues.GetValue(random.Next(initialValues.Length));
                        //_contexts.game.CreateEntity().AddBubble(Guid.NewGuid(), bubbleColor, value, i, j);
                        _contexts.game.CreateEntity().AddMakeBubble(i, j, value);
                    }
                }
            }

            _contexts.game.ReplaceGameState(GameState.Play);
            _contexts.game.isReadyToShot = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;

namespace Assets.Scripts.Systems.Gameplay
{
    public class GameEntityDestroySystem : ReactiveSystem<GameEntity>
    {
        public GameEntityDestroySystem(Contexts c) : base(c.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.ReadyToDestroy);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isReadyToDestroy;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity entity in entities)
            {
                entity.Destroy();
            }
        }
    }
}

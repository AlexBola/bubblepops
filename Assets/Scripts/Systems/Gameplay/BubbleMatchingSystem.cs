﻿using System.Collections.Generic;
using System.Linq;
using Entitas;
using UnityEngine;

namespace Assets.Scripts.Systems.Gameplay
{
    public class BubbleMatchingSystem : ReactiveSystem<GameEntity>
    {
        private readonly Contexts _contexts;

        public BubbleMatchingSystem(Contexts c) : base(c.game)
        {
            _contexts = c;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(Matcher<GameEntity>.AnyOf(GameMatcher.CheckForMatching, GameMatcher.Matched));
        }

        protected override bool Filter(GameEntity entity)
        {
            return true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity entity in entities)
            {
                entity.isCheckForMatching = false;
                CheckForMatching(entity);
            }
        }

        private void CheckForMatching(GameEntity entity)
        {
            var row = entity.bubble.Row;
            var column = entity.bubble.Column;
            Vector2Int[] arr = ((column + 1 & 1) == 0)
                ? new Vector2Int[]
                {
                    new Vector2Int(row, column - 1), 
                    new Vector2Int(row + 1, column - 1),
                    new Vector2Int(row - 1, column),
                    new Vector2Int(row + 1, column),
                    new Vector2Int(row, column + 1),
                    new Vector2Int(row + 1, column + 1)
                }
                : new Vector2Int[]
                {
                    new Vector2Int(row-1, column-1),
                    new Vector2Int(row, column-1),
                    new Vector2Int(row-1, column),
                    new Vector2Int(row+1, column),
                    new Vector2Int(row-1, column+1),
                    new Vector2Int(row, column+1),
                };

            var sameNeighbors = _contexts.game.GetGroup(GameMatcher.Bubble).GetEntities().Where(predicate: e => arr.Contains(new Vector2Int(e.bubble.Row, e.bubble.Column))).Where(n=> n.bubble.Value == entity.bubble.Value && !n.isMatched).ToArray();
            foreach (GameEntity neighbor in sameNeighbors)
            {
                    neighbor.isMatched = true;
            }

            if (sameNeighbors.Length < 1)
                _contexts.game.isMatchingComplete = true;
        }
    }
}

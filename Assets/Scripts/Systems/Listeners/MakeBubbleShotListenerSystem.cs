﻿using Assets.Scripts.Systems.Listeners;
using Entitas;

public class MakeBubbleShotListenerSystem : AGameListenerSystem
{
    public MakeBubbleShotListenerSystem(Contexts c) : base(c) {}

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.MakeShot);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entitas;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scripts.Systems.Gameplay
{
    public class MakeMoveBubblesWaveSystem : ReactiveSystem<GameEntity>
    {
        private readonly Contexts _contexts;

        public MakeMoveBubblesWaveSystem(Contexts c) : base(c.game)
        {
            _contexts = c;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.MakeWave);
        }

        protected override bool Filter(GameEntity entity)
        {
            return true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity entity in entities)
            {
                Debug.Log("MakeMoveBubblesWaveSystem.Execute");
                Random random = new Random();
                int[] initialValues = new[] {2, 4, 8, 16, 32};


                foreach (var bubbleE in _contexts.game.GetEntities(GameMatcher.Bubble))
                {
                    bubbleE.ReplaceBubble(bubbleE.bubble.Key,
                        bubbleE.bubble.Color, bubbleE.bubble.Value,
                        bubbleE.bubble.Row + entity.makeWave.Step,
                        bubbleE.bubble.Column);
                }

                for (var column = 0; column < _contexts.game.gridSize.Columns; column++)
                {
                    int value = (int) initialValues.GetValue(random.Next(initialValues.Length));
                    _contexts.game.CreateEntity().AddMakeBubble(0, column, value);
                }
            }
        }
    }
}

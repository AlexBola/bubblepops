﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entitas;
using UniRx;

public class BubbleShotHandlerSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts    _contexts;

    public BubbleShotHandlerSystem(Contexts c) : base(c.game)
    {
        _contexts = c;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.MakeShot);
    }

    protected override bool Filter(GameEntity entity)
    {
        return _contexts.game.isReadyToShot;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        _contexts.game.isReadyToShot = false;
        //Observable.Timer(TimeSpan.FromSeconds(0.6f)).Subscribe(_ => MakeWave());
        
    }

    private void MakeWave()
    {
        //_contexts.game.ReplaceMakeWave(1);
    }
}

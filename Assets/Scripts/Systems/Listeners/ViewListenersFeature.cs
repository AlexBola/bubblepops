﻿using UnityEngine;
using System.Collections;

public class ViewListenersFeature : Feature
{
    public ViewListenersFeature(Contexts c)
    {
        Add(new GridSizeListenerSystem(c));
        Add(new GameStateListenerSystem(c));
        Add(new MakeBubbleShotListenerSystem(c));
        Add(new GameBubbleUpdateListenerSystem(c));
        Add(new MakeWaveListenerSystem(c));
        Add(new BubbleBlowListenerSystem(c));
    }
}

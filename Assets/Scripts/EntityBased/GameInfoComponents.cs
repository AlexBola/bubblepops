﻿using UnityEngine;
using System.Collections;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[GameInfo, Unique]
public class GameScreenComponent : IComponent
{
    public GameScreen CurrentScreen;
}

public enum GameScreen
{
    Menu,
    Gameplay
}

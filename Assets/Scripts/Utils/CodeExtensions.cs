﻿using System;
using UnityEngine;
using System.Collections;

public static class DelegateExtensions
{
    public static void SafeInvoke(this Action @this)
    {
        if (@this != null)
            @this.Invoke();
    }
    public static void SafeInvoke<T>(this Action<T> @this, T param)
    {
        if (@this != null)
            @this.Invoke(param);
    }

    public static int HexToDex(this string @this)
    {
        return System.Convert.ToInt32(@this, 16);
    }

    public static string DexToHex(this int @this)
    {
        return @this.ToString("X2");
    }

    public static string FloatToNormalized(this float @this)
    {
        return Mathf.RoundToInt(@this * 255f).DexToHex();
    }

    public static float HexToFloatNormalized(this string @this)
    {
        return @this.HexToDex() / 255f;
    }

    public static Color GetColorFromString(this string @this)
    {
        float red = @this.Substring(0, 2).HexToFloatNormalized();
        float green = @this.Substring(2, 2).HexToFloatNormalized();
        float blue = @this.Substring(4, 2).HexToFloatNormalized();
        float alpha = 1f;
        if (@this.Length > 8)
            alpha = @this.Substring(6, 2).HexToFloatNormalized();
        return new Color(red, green, blue, alpha);
    }

    public static string GetStringFromColor(this Color @this, bool useAlpha = false)
    {
        string red = FloatToNormalized(@this.r);
        string green = FloatToNormalized(@this.g);
        string blue = FloatToNormalized(@this.b);
        if (!useAlpha)
            return red + green + blue;
        string alpha = FloatToNormalized(@this.a);
        return red + green + blue + alpha;
    }
}
﻿using Assets.Scripts.Systems;
using Assets.Scripts.Systems.Gameplay;

public class GameplayFeature : Feature
{
    public GameplayFeature(Contexts c)
    {
        Add(new InitGameSystem(c));
        Add(new InitiateGameFieldSystem(c));
        Add(new CreateBubbleSystem(c));
        Add(new GameStartSystem(c));
        Add(new MakeMoveBubblesWaveSystem(c));
        Add(new BubbleShotHandlerSystem(c));
        Add(new BubbleMatchingSystem(c));
        Add(new CheckForMatchingResultSystem(c));
        Add(new AddTargetSystem(c));
        Add(new GameEntityDestroySystem(c));
    }
}

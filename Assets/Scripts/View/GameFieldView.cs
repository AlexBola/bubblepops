﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Entitas;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class GameFieldView : MonoBehaviour
{
    [SerializeField]
    private     GameObject      _bubbleInctance;
    [SerializeField]
    private     Vector2         _initialPosition;
    [SerializeField]
    private     Transform      _container;
    [SerializeField]
    private     Transform      _inactiveContainer;

    [SerializeField]
    private     LineRenderer    _lineRenderer;

    private     Vector2         _direction;

    private     BubbleView[,]   _grid;
    private     Contexts        _contexts;

    private     float           _bubbleBound;
    private     float           _xInit;
    private     float           _yInit;

    
    private     static readonly Predicate<int>  Predicate1 = i => (i + 1 & 1) == 0;
    private     static readonly Predicate<int>  Predicate2 = i => (i + 1 & 1) != 0;
    private     Predicate<int> _isIndent = Predicate1;

    private     static float _rowIndent      =    0.5f;
    private     static float _columnIndent   =    0.9f;
    private     static float _rootXIndent    =    -2.7f;
    private     static float _rootYIndent    =    5.7f;
    private     static float _shotMoveDelay  =    0.3f;

    private readonly Dictionary<Guid, BubbleView> _activeBubbles = new Dictionary<Guid, BubbleView>();
    private readonly Stack<BubbleView> _pool = new Stack<BubbleView>();

    // Start is called before the first frame update
    void Start()
    {
        _initialPosition = _bubbleInctance.transform.position;
        _bubbleBound = _bubbleInctance.GetComponent<CircleCollider2D>().radius * 2;
        _xInit = _bubbleBound * _rootXIndent;
        _yInit = _bubbleBound * _rootYIndent;

        Subscribe();
    }

    private void Subscribe()
    {
        _contexts = Contexts.sharedInstance;
        List<MatchingData<GameEntity>> observersList = new List<MatchingData<GameEntity>>()
        {new MatchingData<GameEntity>
        {
            Matcher = GameMatcher.GridSize,
            Filter = (e) => e.hasGridSize,
            UpdateView = OnGridUpdate
        }, new MatchingData<GameEntity>
            {
                Matcher = GameMatcher.GameState,
                Filter = (e) => e.hasGameState && 
                                e.gameState.CurrentState == GameState.Play,
                UpdateView = OnGameStateUpdate
            },
            new MatchingData<GameEntity>
            {
                Matcher = GameMatcher.MakeShot,
                Filter = (e) => e.hasMakeShot,
                UpdateView = OnMakeShot
            },
            new MatchingData<GameEntity>
            {
            Matcher = GameMatcher.Bubble,
            Filter = (e) => e.hasBubble && !_activeBubbles.ContainsKey(e.bubble.Key),
            UpdateView = InitBubble
            },
            new MatchingData<GameEntity>
            {
            Matcher = GameMatcher.Bubble,
            Filter = (e) => e.hasBubble && e.hasTarget && _activeBubbles.ContainsKey(e.bubble.Key),
            UpdateView = ReplaceTarget
            },
            new MatchingData<GameEntity>
            {
            Matcher = GameMatcher.MakeWave,
            Filter = (e) => e.hasMakeWave,
            UpdateView = OnWaveUpdate
            },
            new MatchingData<GameEntity>
            {
                Matcher = GameMatcher.Blow,
                Filter = (e) => e.isBlow,
                UpdateView = OnBubbleBlow
            }
        };
        _contexts.view.CreateEntity().AddViewListener(observersList);
    }

    private void OnBubbleBlow(GameEntity entity)
    {
        var view = _activeBubbles[entity.bubble.Key];
        view.Destroy();
        _activeBubbles.Remove(entity.bubble.Key);
        _pool.Push(view);
        view.transform.SetParent(_inactiveContainer);
        view.transform.position = new Vector3(-100, 0, 0);
        entity.isReadyToDestroy = true;
    }

    private void OnWaveUpdate(GameEntity waveE)
    {
        _isIndent = _isIndent == Predicate1 ? Predicate2 : Predicate1;
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction;
        DrawLine();
    }

    public void OnGridUpdate(GameEntity entity)
    {
        Debug.Log(string.Format("GameFieldView.ViewUpdate rows: {0} columns: {1}", entity.gridSize.Rows, entity.gridSize.Columns));
        _grid = new BubbleView[entity.gridSize.Rows, entity.gridSize.Columns];
    }

    public void OnGameStateUpdate(GameEntity entity)
    {
    }

    public void OnMakeShot(GameEntity entity)
    {
        MakeShot(entity);
    }

    public void MakeShot(GameEntity entity)
    {
        var view = _activeBubbles[entity.bubble.Key];
        var go = view.transform.gameObject;
        go.transform.position = _initialPosition;
        view.Collider.enabled = true;
        // Make bubble move.
        go.transform.DOMove(GetPositionByCell(entity.bubble.Row, entity.bubble.Column), _shotMoveDelay);
        entity.RemoveTarget();
        entity.RemoveMakeShot();
        // Hide tracer line.
        _lineRenderer.positionCount = 0;
        // Check for matching when move completed.
        Observable.Timer(TimeSpan.FromSeconds(_shotMoveDelay)).Subscribe(_ => entity.isCheckForMatching = true);
    }

    private void DrawLine()
    {
        // Draw tracer line.
        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPosition(0, _initialPosition);
        _lineRenderer.SetPosition(1, _direction * 10);
    }

    private void InitBubble(GameEntity bubbleE)
    {
        var go = GetBubbleInstance();
        var view = go.GetComponent<BubbleView>();
        //_grid[bubbleE.bubble.Row, bubbleE.bubble.Column] = view;
        view.Init(bubbleE, new Vector2(_xInit, _yInit));
        view.Collider.enabled = !bubbleE.hasTarget;
        go.transform.position = GetPositionByCell(bubbleE.bubble.Row, bubbleE.bubble.Column);
        _activeBubbles.Add(bubbleE.bubble.Key, view);
        bubbleE.RemoveMakeBubble();
    }

    public void ReplaceTarget(GameEntity bubbleE)
    {
        var view = _activeBubbles[bubbleE.bubble.Key];
        view.transform.position = GetPositionByCell(bubbleE.bubble.Row, bubbleE.bubble.Column);
    }

    private Vector3 GetPositionByCell(int row, int column)
    {
        var x = _xInit + _bubbleBound * column;
        var y = _yInit - _bubbleBound * _columnIndent * row;
        if (!_isIndent(row))
            x += _bubbleBound * _rowIndent;
        return new Vector3(x, y);
    }

    private GameObject GetBubbleInstance()
    {
        if (_pool.Count > 0)
        {
            return _pool.Pop().gameObject;
        }
        return Instantiate(_bubbleInctance, _initialPosition, _bubbleInctance.transform.rotation, _container);
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public MakeShotComponent makeShot { get { return (MakeShotComponent)GetComponent(GameComponentsLookup.MakeShot); } }
    public bool hasMakeShot { get { return HasComponent(GameComponentsLookup.MakeShot); } }

    public void AddMakeShot(UnityEngine.Vector3 newDirection) {
        var index = GameComponentsLookup.MakeShot;
        var component = (MakeShotComponent)CreateComponent(index, typeof(MakeShotComponent));
        component.Direction = newDirection;
        AddComponent(index, component);
    }

    public void ReplaceMakeShot(UnityEngine.Vector3 newDirection) {
        var index = GameComponentsLookup.MakeShot;
        var component = (MakeShotComponent)CreateComponent(index, typeof(MakeShotComponent));
        component.Direction = newDirection;
        ReplaceComponent(index, component);
    }

    public void RemoveMakeShot() {
        RemoveComponent(GameComponentsLookup.MakeShot);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherMakeShot;

    public static Entitas.IMatcher<GameEntity> MakeShot {
        get {
            if (_matcherMakeShot == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.MakeShot);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherMakeShot = matcher;
            }

            return _matcherMakeShot;
        }
    }
}

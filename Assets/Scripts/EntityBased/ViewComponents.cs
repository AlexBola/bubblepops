﻿using System;
using System.Collections.Generic;
using Entitas;

[View]
public class ViewListenerComponent : IComponent
{
    public List<MatchingData<GameEntity>> Observables;
}

public class MatchingData<TEntity> where TEntity : class, IEntity
{
    public IMatcher<TEntity> Matcher;
    public Func<TEntity, bool> Filter;
    public Action<TEntity> UpdateView;
}

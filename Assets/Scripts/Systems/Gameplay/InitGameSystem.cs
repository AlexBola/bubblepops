﻿using Entitas;

namespace Assets.Scripts.Systems.Gameplay
{
    public class InitGameSystem : IInitializeSystem
    {
        private readonly Contexts _contexts;

        public InitGameSystem(Contexts c)
        {
            _contexts = c;
        }

        public void Initialize()
        {
            _contexts.game.CreateEntity().AddGridSize(10, 6);
            _contexts.gameInfo.ReplaceGameScreen(GameScreen.Gameplay);
        }
    }
}

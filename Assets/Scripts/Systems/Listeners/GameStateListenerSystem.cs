﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Systems.Listeners;
using Entitas;

public class GameStateListenerSystem : AGameListenerSystem
{
    public GameStateListenerSystem(Contexts c) : base(c){}

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.GameState);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }
}
